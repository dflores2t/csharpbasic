﻿namespace Array;

public class program{
    static void Main(string[] arg){
        //CONSTRUYENDO ARREGLOS
        double[] calif = new double[3];

        double promedio = 0.0;
        double sumatoria = 0.0;
        double diferencia = 0.0;
        int n = 0;
        string dato = "";

        //pedimos las calificaciones
        for (n = 0; n < 3; n++)
        {
            Console.WriteLine("DAME LA CALIFICACION");
            calif[n] = Convert.ToDouble(Console.ReadLine());
        }

        //calculamos el promedio
        for (n = 0; n < 3; n++){
            sumatoria += calif[n];
        }
        promedio = sumatoria / calif.Length;

        //calculamos la diferoncia e imprimimos
        for (n = 0; n < 3; n++){
            diferencia = promedio - calif[n];
            Console.WriteLine($"la calificacion es: {calif[n]}, el promedio es {promedio} y su diferencia es {diferencia}");
        }
    }
}
