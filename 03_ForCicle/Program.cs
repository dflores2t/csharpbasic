﻿namespace ForCicle;

public class program{
    static void Main(string[] arg){
        Console.WriteLine("CONTROL DE FLUJO - FOR CICLE");
        //ESTRUCTURA - REPETITIVAS - PARTES CICLOS FOR
        double n1 = 0.0;
        double n2 = 0.0;
        double n3 = 0.0;
        double promedio = 0.0;
        string dato= "";

        //GET NUMBER 1
        // Console.WriteLine(" Dame el numero 1:");
        // dato = Console.ReadLine();
        // n1 = Convert.ToDouble(dato);
        // Console.WriteLine(" Dame el numero 2:");
        // dato = Console.ReadLine();
        // n2 = Convert.ToDouble(dato);
        // Console.WriteLine(" Dame el numero 3:");
        // dato = Console.ReadLine();
        // n3 = Convert.ToDouble(dato);
        double suma = 0.0;
        int cant = 0;
        Console.WriteLine("How many number you want to sum");
        cant = Convert.ToInt32(Console.ReadLine());
        //ciclo for
        for (int i = 1; i <= cant; i++)
        {
            Console.WriteLine($"Give me number :{i}");
             suma += Convert.ToDouble(Console.ReadLine());
            promedio = suma / i;
        }

        //CALCULO DE PROMEDIO
        // promedio = suma / 3;

        //SHOW RESULT
        Console.WriteLine($"AVERAGE IS : {promedio}");
        Console.Write(" Press any key to exit...");
        Console.ReadKey();
    }
}

