﻿namespace While;

public class program{
    static void Main(string[] arg){
        //PROGRAMA QUE MUESTRA EL CICLO WHILE
        //PUEDE O NO LLEVARSE A CABO NI UNA VEZ
        //SE USA CUANDO NO SABEMOS EL NUMERO DE REPETICIONES

        //VARIABLE
        int dinero = 0;
        int opcion = 0;
        string dato = "";
        int total = 0;

        //pedimos la cantidad de dinero
        Console.WriteLine("Cuanto Dinero Tienes?");
        dinero = Convert.ToInt32(Console.ReadLine());

        while(dinero > 0 && opcion !=5){
            //presetamos el menu
            Console.WriteLine("1.Dulce, 2.Papas, 3.Chocolates, 4.Helado,5.Salir");
            opcion = Convert.ToInt32(Console.ReadLine());

            switch(opcion){
                case 1:
                    dinero -= 3;
                    total += 3;
                    break;
                case 2:
                    dinero -= 12;
                    total += 12;
                    break;
                case 3:
                    dinero -= 7;
                    total += 7;
                    break;
                case 4:
                    dinero -= 25;
                    total += 25;
                    break;
            }
            Console.WriteLine($" Tienes {dinero} y has gastado: {total}");
        }
        Console.Write("Adios");
    }
}