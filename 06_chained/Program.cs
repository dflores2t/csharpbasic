﻿namespace chained;

public class program{
    static void Main(string[] arg){
        //ciclos anidados

        int n = 0;
        int m = 0;
        int producto = 0;
        bool primo = true;

        //imprimir la tabla de multiplicar
        // for (n=1; n <=10; n++)
        // {
        //     for (m = 1; m <=10; m++)
        //     {
        //         producto = n * m;
        //         Console.WriteLine($"{n} X {m} = {producto}");
        //     }
        //     Console.WriteLine();
        // }

        for (n = 2; n < 100; n++)
        {
            primo = true;
            for (m = 2; m < n; m++){
                if(n % m ==0){
                    primo = false;
                }
            }
            if(primo == true){
                Console.Write($"{n} ,");
            }
        }
        Console.WriteLine();
    }
}