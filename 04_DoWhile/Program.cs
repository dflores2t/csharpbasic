﻿namespace DoWhile;

class program {
    static void Main(string[] arg){
        //DO WHILE
        //PARTES DEL CICLO
        //COMO FUNCIONA

        /*
            progara que muestra el uso del ciclo do while
            se lleva a caba cuando menos una vez
            se usa cando no sabemos la cantidad de repeticione

        
        */

        int opcion = 0;
        string dato = "";
        double tipoCambio = 0.0;
        double pesos = 0.0;
        double dolar = 0.0;

        //hacemos el ciclo do while
        do
        {
            //menu
            Console.WriteLine("1.C$ To $, 2.$ to C$, 3. Exit");
            opcion = Convert.ToInt32(Console.ReadLine());

            switch(opcion){
                case 1:
                    Console.WriteLine("Give Me The mount int C$ to Convert");
                    pesos = Convert.ToDouble(Console.ReadLine());
                    tipoCambio = pesos * 35.5;
                    Console.WriteLine($"Son {tipoCambio} Cordobas");
                    break;
                case 2:
                    Console.WriteLine("Give Me The Mount in $ to Convert");
                    dolar = Convert.ToDouble(Console.ReadLine());
                    tipoCambio = dolar / 35.5;
                    Console.WriteLine($"Son {tipoCambio} Dolar");
                    break;
                default:
                    Console.WriteLine("Option Invalid");
                    break;
            }
        

        } while (opcion != 3);

    }
}